#ifndef __DSM_GLOBAL_H__
#define __DSM_GLOBAL_H__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <vector>
#include <string>
#include <mutex>

#include "logger.h"

namespace dsm {

const int PAGE_SIZE        = 4096;
const int LISTEN_QUEUE_LEN = 10;
const int MAX_IP_LEN       = 32;

// PageTable Info
struct ClientInfo {
    std::string ip;
    int port;
};

struct PageInfo {
    bool allocated{false};
    ClientInfo owner;
    std::vector<ClientInfo> copy_set;
};

enum PageStatus {
    PAGE_NONE,  PAGE_WRITE_READ, PAGE_READ_ONLY
};

// Message
enum MessageType {
    MSG_REGISTER_CLIENT, MSG_REGISTER_CLIENT_REPLY,
    MSG_REQUEST_PAGE, MSG_REQUEST_PAGE_REPLY, MSG_REQUEST_DONE,
    MSG_CHANGE_OWNER, MSG_CHANGE_OWNER_REPLY,
};

struct MessageHead {
    MessageType type;
    size_t len;
};

} // namespace dsm

#endif // __DSM_GLOBAL_H__
