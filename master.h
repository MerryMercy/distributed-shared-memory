#ifndef __MASTER_H__
#define __MASTER_H__

#include "global.h"
#include "socket.h"
#include <sys/mman.h>
#include <thread>

namespace dsm {

class Master {
public:
    Master(std::string ip, int port, size_t n_page) : ip_(ip), port_(port),
                    page_info_(n_page), page_lock_(n_page), n_page_(n_page) {
        // create socket server
        listen_fd_ = start_server(port);
        curr_pt = 0;
    }

    void serve() {
        // map shared memory
        shared_region_ = static_cast<unsigned char *>(mmap((void*)NULL, n_page_ * PAGE_SIZE,
                             PROT_NONE, MAP_SHARED | MAP_ANONYMOUS, -1, 0));

        LOG(INFO) << "Master's shared memory begins at " << static_cast<void *>(shared_region_);

        // start server thread
        thread_ = std::thread(&Master::run_, this);
    }

    ~Master() {
        thread_.join();
    }

private:
    // listen to port
    void run_() {
        socklen_t cli_len = sizeof(struct sockaddr_in);
        struct sockaddr_in cli_addr;
        int conn_fd;
        MessageHead head;
        int count;

        while (1) {
            LOG(INFO) << "Master is waiting at " << ip_ << ":" << port_;

            memset(&cli_addr, 0, sizeof(cli_addr));
            conn_fd = accept(listen_fd_, (struct sockaddr *)&cli_addr, &cli_len);
            if (conn_fd == -1) {
                LOG(WARNING) << "ah?? pass";
                continue;
            }

            count = recv(conn_fd, &head, sizeof(head), 0);
            if (count != sizeof(head)) {
                LOG(ERROR) << "read error";
            }

            process_(head, conn_fd);
        }
    }

    // decode message and process
    void process_(MessageHead head, int conn_fd) {
        switch (head.type) {
            case MSG_REGISTER_CLIENT: {
                // new client register, allocate pages to it
                size_t n_hold;
                char ip_buf[MAX_IP_LEN];
                std::string client_ip;
                int client_port;

                recv(conn_fd, &n_hold, sizeof(n_hold), 0);
                recv(conn_fd, ip_buf, sizeof(ip_buf), 0);
                recv(conn_fd, &client_port, sizeof(client_port), 0);

                client_ip = std::string(ip_buf);

                LOG(INFO) << "[REGISTER] new client " << client_ip << ":" << client_port
                          << " enters, numter of hold page: " << n_hold;

                register_lock.lock();
                n_hold = std::min(n_hold, n_page_ - curr_pt);

                // mark pages and make response
                int *alloc_pages = new int[n_hold];
                for (size_t i = 0; i < n_hold; i++) {
                    page_lock_[curr_pt + i].lock();
                    page_info_[curr_pt + i].allocated=true;
                    page_info_[curr_pt + i].owner = ClientInfo{client_ip, client_port};
                    alloc_pages[i] = curr_pt + i;
                }

                // send response
                head.type = MSG_REGISTER_CLIENT_REPLY;
                head.len  = sizeof(void *) + n_hold * sizeof(int);

                send(conn_fd, &head, sizeof(head), 0);
                send(conn_fd, &shared_region_, sizeof(shared_region_), 0);
                send(conn_fd, alloc_pages, sizeof(int) * n_hold, 0);

                delete [] alloc_pages;

                // unlock
                for (size_t i = 0; i < n_hold; i++) {
                    page_lock_[curr_pt + i].unlock();
                }
                curr_pt += n_hold;
                register_lock.unlock();

                LOG(INFO) << "[REGISTER] done";

                break;
            }

            default:
                LOG(FATAL) << "unkonwn message type: " << head.type;
        }
    }

    /************* MEMBER VARIABLE *************/
    // address
    std::string ip_;
    int port_;

    // pages
    std::vector<PageInfo>   page_info_;
    std::vector<std::mutex> page_lock_;
    size_t n_page_;   // total number of pages
    size_t curr_pt;   // pointer to current allocated top page

    // memory
    unsigned char *shared_region_;

    // socket
    int listen_fd_;
    std::thread thread_;

    // other locks
    std::mutex register_lock;
};

} // namespace dsm

#endif // __MASTER_H__
