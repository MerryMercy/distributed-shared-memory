#include "socket.h"

#include <cstring>

#include "global.h"

namespace dsm {

int start_server(int port) {
    int val = 1;
    int listen_fd;
    int ret;
    struct sockaddr_in server_addr;

    listen_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_fd == -1) {
        LOG(FATAL) << "socket call failed";
    }

    ret = setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    if (ret == -1) {
        LOG(FATAL) << "setsockopt call failed";
    }

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(port);
    ret = bind(listen_fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (ret == -1) {
        LOG(FATAL) << "bind call failed";
    }
    ret = listen(listen_fd, LISTEN_QUEUE_LEN);
    if (ret == -1) {
        LOG(FATAL) << "listen call failed";
    }

    return listen_fd;
}

int connect_to_server(std::string ip, int port) {
    int conn_fd;
    struct sockaddr_in server_addr;

    conn_fd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port   = htons(port);
    inet_pton(AF_INET, ip.c_str(), &server_addr.sin_addr);
    while (connect(conn_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) != 0)
        ;
    return conn_fd;
}

} // namespace dsm
