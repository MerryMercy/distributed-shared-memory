#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <sstream>
#include <iostream>

namespace dsm {

/**
 * Simple Logger
 */
template <bool throw_exception=false>
class Logger
{
public:
    Logger(const char *filename, int line) {
        if (filename != nullptr)
            buffer << filename << ":" << line << " : ";
    }

    template <typename T>
    Logger& operator<<(T const & value) {
        buffer << value;
        return *this;
    }

    ~Logger() noexcept(false) {
        if (throw_exception)
            // dangerous, but the program is already fatal
            throw std::runtime_error(buffer.str());
        else {
            // This is atomic according to the POSIX standard
            // http://www.gnu.org/s/libc/manual/html_node/Streams-and-Threads.html
            std::cerr << buffer.str() << std::endl;
        }
    }

private:
    std::ostringstream buffer;
};


#define LOG(level) LOG_##level

#ifndef LOG_WARNING_ENABLE
#define LOG_WARNING_ENABLE 1
#endif

#ifndef LOG_INFO_ENABLE
#define LOG_INFO_ENABLE 1
#endif

#define LOG_FATAL   dsm::Logger<true>(__FILE__, __LINE__)
#define LOG_ERROR   dsm::Logger<>(__FILE__, __LINE__)
#define LOG_WARNING if (!LOG_WARNING_ENABLE) ; else dsm::Logger<>(__FILE__, __LINE__)
#define LOG_INFO    if (!LOG_INFO_ENABLE) ; else dsm::Logger<>(__FILE__, __LINE__)

} // namespace dsm

#endif // __LOGGER_H__

