#ifndef __CLIENT_H__
#define __CLIENT_H__

#include <signal.h>
#include <sys/mman.h>
#include <thread>

#include "global.h"
#include "socket.h"

namespace dsm {

class Client {
public:
    Client(std::string ip, int port, std::string master_ip, int master_port, size_t n_page, 
           size_t n_hold_page) : ip_(ip), port_(port), master_ip_(master_ip),
               master_port_(master_port), page_status_(n_page), page_lock_(n_page),
               n_page_(n_page), n_hold_page_(n_hold_page) {
        for (size_t i = 0; i < n_page_; i++)
            page_status_[i] = PAGE_NONE;

        listen_fd_ = start_server(port);
        me = this;
   }

    void serve() {
        add_pagefault_handler_();
        register_to_master_();

        // start server thread
        thread_ = std::thread(&Client::run_, this);
    }

    unsigned char *get_shared() {
        return shared_region_;
    }

    void handler(int sig, siginfo_t *si, void *unused) {
        int page_no = (static_cast<unsigned char*>(si->si_addr) - shared_region_) / PAGE_SIZE;
        LOG(INFO) << "page fault on " << si->si_addr << ", page: " << page_no;

        page_lock_[page_no].lock();

        // send request page
        switch (page_status_[page_no]) {
            case PAGE_NONE: {
                unsigned char page_buf[PAGE_SIZE];
                char ip_buf[MAX_IP_LEN];
                std::string owner_ip;
                int  owner_port;

                int master_fd, owner_fd;

                MessageHead head;
                // connect to master
                master_fd = connect_to_server(master_ip_, master_port_);

                // send page no
                head.type = MSG_REQUEST_PAGE;
                head.len  = sizeof(page_no);
                send(master_fd, &head, sizeof(head), 0);
                send(master_fd, &page_no, sizeof(page_no), 0);

                // receive owner client address
                recv(master_fd, &head, sizeof(head), 0);
                assert(head.type == MSG_REQUEST_PAGE_REPLY);
                recv(master_fd, ip_buf, sizeof(ip_buf), 0);
                owner_ip = std::string(ip_buf);
                recv(master_fd, &owner_port, sizeof(owner_port), 0);

                // connect to owner
                owner_fd = connect_to_server(owner_ip, owner_port);

                // send to owner
                head.type = MSG_CHANGE_OWNER;
                head.len  = sizeof(page_no);
                send(owner_fd, &head, sizeof(head), 0);
                send(owner_fd, &page_no, sizeof(page_no), 0);

                // receive from owner
                recv(owner_fd, &head, sizeof(head), 0);
                assert(head.type == MSG_CHANGE_OWNER_REPLY);
                recv(owner_fd, page_buf, sizeof(page_buf), 0);

                // send done to master
                head.type = MSG_REQUEST_DONE;
                send(master_fd, &head, sizeof(head), 0);

                // mark own data
                page_status_[page_no] = PAGE_WRITE_READ;
                mprotect(shared_region_ + static_cast<size_t>(PAGE_SIZE) * page_no,
                                            PAGE_SIZE, PROT_READ | PROT_WRITE);
                memcpy(shared_region_ + static_cast<size_t>(PAGE_SIZE) * page_no,
                       page_buf, PAGE_SIZE);
                break;
            }

            case PAGE_READ_ONLY:
            case PAGE_WRITE_READ:
            default:
                LOG(FATAL) << "cannot happen";
        }

        page_lock_[page_no].unlock();

    }

    static void static_handler(int sig, siginfo_t *si, void *unused) {
        me->handler(sig, si, unused);
    }

    ~Client() {
        thread_.join();
    }

private:
    // listen to port
    void run_() {
        socklen_t cli_len = sizeof(struct sockaddr_in);
        struct sockaddr_in cli_addr;
        int conn_fd;
        MessageHead head;
        int count;

        while (1) {
            LOG(INFO) << "Client is waiting at " << ip_ << ":" << port_;

            memset(&cli_addr, 0, sizeof(cli_addr));
            conn_fd = accept(listen_fd_, (struct sockaddr *)&cli_addr, &cli_len);
            if (conn_fd == -1) {
                LOG(WARNING) << "ah?? pass";
                continue;
            }

            count = recv(conn_fd, &head, sizeof(head), 0);
            if (count != sizeof(head)) {
                LOG(ERROR) << "read error";
            }

            process_(head, conn_fd);
        }
    }

    // decode message and process
    void process_(MessageHead head, int conn_fd) {
        LOG(INFO) << head.type << " " << head.len;
    }

    // add page fault handler
    void add_pagefault_handler_() {
        struct sigaction sa;

        sa.sa_flags = SA_SIGINFO;
    	sigemptyset(&sa.sa_mask);
    	sa.sa_sigaction = static_handler;
    	if (sigaction(SIGSEGV, &sa, NULL) == -1) {
            LOG(FATAL) << "hook page fault error";
        }
    }

    // register to master, get address of shared memory
    void register_to_master_() {
        // register
        int conn_fd = connect_to_server(master_ip_, master_port_);
        char ip_buf[MAX_IP_LEN];

        MessageHead head;
        head.type = MSG_REGISTER_CLIENT;
        head.len  = sizeof(int);

        // send 
        send(conn_fd, &head, sizeof(head), 0);
        send(conn_fd, &n_hold_page_, sizeof(n_hold_page_), 0);
        strcpy(ip_buf, ip_.c_str());
        send(conn_fd, ip_buf, sizeof(ip_buf), 0);
        send(conn_fd, &port_, sizeof(port_), 0);

        // recv
        recv(conn_fd, &head, sizeof(head), 0);
        assert(head.type == MSG_REGISTER_CLIENT_REPLY);
        recv(conn_fd, &shared_region_, sizeof(shared_region_), 0);

        // create mmap
        shared_region_ = static_cast<unsigned char*>(mmap(shared_region_, n_page_ * PAGE_SIZE,
                             PROT_NONE, MAP_SHARED | MAP_ANONYMOUS, -1, 0));

        LOG(INFO) << "Client's shared memory begins at " << (void *) shared_region_;

        n_hold_page_ = (head.len - sizeof(shared_region_)) / sizeof(int);
        int *alloc_pages = new int[n_hold_page_];
        recv(conn_fd, alloc_pages, sizeof(int) * n_hold_page_, 0);
        for (size_t i = 0; i < n_hold_page_; i++) {
            int page_no = alloc_pages[i];
            page_lock_[page_no].lock();
            page_status_[page_no] = PAGE_WRITE_READ;
            mprotect(shared_region_ + static_cast<size_t>(PAGE_SIZE) * page_no,
                                            PAGE_SIZE, PROT_READ | PROT_WRITE);
            page_lock_[page_no].unlock();
        }

        close(conn_fd);
    }

    /************* MEMBER VARIABLE *************/
    // address
    std::string ip_;
    int port_;
    std::string master_ip_;
    int master_port_;

    // pages
    std::vector<PageStatus> page_status_;
    std::vector<std::mutex> page_lock_;
    size_t n_page_, n_hold_page_;

    // memory
    unsigned char *shared_region_;

    // socket
    int listen_fd_;
    std::thread thread_;

    static Client* me;  // static 'this' pointer for signal handler
};

} // namespace dsm

#endif // __CLIENT_H__



