#include <iostream>

#include "global.h"
#include "logger.h"
#include "master.h"
#include "client.h"

using namespace dsm;

const int DEFAULT_TOTAL_PAGE_NUMBER  = 1 << 20;
const int DEFAULT_CLIENT_PAGE_NUMBER = 1 << 16;

Client* Client::me; // static 'this' pointer for signal handler


void test_simple_count(int id, unsigned char *addr) {
    // two clients count numbers
    size_t n = 10;
    if (id == 0) {
        ;
    } else if (id == 1) {
        LOG(INFO) << "client 1 write";
        for (size_t i = 0; i < n; i += 1) {
            addr[i] = i;
            LOG(INFO) << (void *)(addr + i) << " " << i;
        }
        addr[n] = 0x93;

        LOG(INFO) << "client 1 write done";
    } else if (id == 2) {
        LOG(INFO) << "client 2 read";

        while (addr[n] != 0x93)
            ;

        for (size_t i = 0; i < n; i += 1) {
            LOG(INFO) << (void*)(addr + i) << " " << i;
        }

        LOG(INFO) << "client 2 read done";
    }
}


int main(int argc, char *argv[]) {
    if (argc != 6) {
        std::cout << "Usage: dsm id ip port master_ip master_port" << std::endl;
        return -1;
    }

    int id = atoi(argv[1]); // id == 0 for master, id >= 1 for clients
    std::string ip = std::string(argv[2]);
    int port   = atoi(argv[3]);
    std::string master_ip = std::string(argv[4]);
    int master_port = atoi(argv[5]);

    int total_page_number = DEFAULT_TOTAL_PAGE_NUMBER;
    int client_page_number = DEFAULT_CLIENT_PAGE_NUMBER;

    Master *master = nullptr;
    Client *client = nullptr;
    unsigned char *addr;

    if (id == 0) {
        LOG(INFO) << "Master starts at " << ip << ":" << port;

        master = new Master(ip, port, total_page_number);
        master->serve();
        addr = nullptr;
    } else {
        LOG(INFO) << "Client starts at " << ip << ":" << port << ", to master " << master_ip << ":" << master_port;

        client = new Client(ip, port, master_ip, master_port, total_page_number,
                                                             client_page_number);
        client->serve();
        addr = client->get_shared();
    }

    test_simple_count(id, addr);

    if (master != nullptr)
        delete master;

    if (client != nullptr)
        delete client;

    return 0;
}
