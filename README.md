# Distributed Shared Memory

## Architecture
Very similar to the sequential consistency and Ivy case study in reference [1].  
See design-note.txt for detailed protocol. It may contain some bugs.

## how to run test
compile (PR on makefile is welcomed)
```bash
g++ test.cc master.cc socket.cc -std=c++11 -pthread -Wall
```

run test
open a terminal, run 
```bash
bash run_master.sh
```
then open two other terminals, run
```bash
bash run_client.sh 1
```
```bash
bash run_client.sh 2
```

You need at least 2 clients to begin test. The master does not use shared memroy. It only stores meta-data.
The tests are similar to reference[2]. We can begin from simple counting. I haven't finishes the first one yet....
But the framework and ideas are all here, very close to finishing it !

## Reference
[1] http://www.cdk5.net/dsm/Ed4/Chapter%2018%20DSM.pdf  
[2] https://github.com/sai25590/Distributed-shared-memory-implementation  
[3] https://github.com/swapniltiwari/Distributed-Shared-Virtual-Memory