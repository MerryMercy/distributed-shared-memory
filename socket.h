#ifndef __SOCKET_H__
#define __SOCKET_H__

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string>
#include <unistd.h>

namespace dsm {

// start server at port, return socket descriptor
int start_server(int port);

// connect to server, return socket descriptor
int connect_to_server(std::string ip, int port);

} // namespace dsm


#endif // __SOCKET_H__

